///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// BrainfuckCodeGeneratorAbstract.cpp - The Brainfuck Generator Class Implementation
///////////////////////////////////////////////////////////////////////////////

#include <memory.h>
#include <string>
#include <assert.h>

#include "..\include\BrainfuckCodeGeneratorAbstract.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

ABrainfuckCodeGenerator::ABrainfuckCodeGenerator()
{
	ResetGenerator();
}
	
ABrainfuckCodeGenerator::~ABrainfuckCodeGenerator() {}

const string & ABrainfuckCodeGenerator::GetCodeString() const
{
	return m_codeStringStr;
}

void ABrainfuckCodeGenerator::ResetGenerator()
{
	m_brainfuckBufferPos = 0;
	m_brainfuckBufferStr.assign( MAX_BUFFER_SIZE, 0x0 );

	m_codeStringStr.clear();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void ABrainfuckCodeGenerator::MoveLeftCodeGen()
{
	m_brainfuckBufferPos--;
	m_codeStringStr += CODE_CHAR__DEC_PTR;
}

void ABrainfuckCodeGenerator::MoveRightCodeGen()
{
	m_brainfuckBufferPos++;
	m_codeStringStr += CODE_CHAR__INC_PTR;
}

void ABrainfuckCodeGenerator::PrintCharCodeGen()
{
	m_codeStringStr += CODE_CHAR__PRINT_CHAR;
}

void ABrainfuckCodeGenerator::IncCharCodeGen()
{
	m_brainfuckBufferStr[m_brainfuckBufferPos]++;
	m_codeStringStr += CODE_CHAR__INC_VAL;
}

void ABrainfuckCodeGenerator::DecCharCodeGen()
{
	m_brainfuckBufferStr[m_brainfuckBufferPos]--;
	m_codeStringStr += CODE_CHAR__DEC_VAL;
}

void ABrainfuckCodeGenerator::WalkToCharCodeGen( char destChar )
{
	bool			goUp;
	unsigned int	charDistance;

	// Calculate distance and direction
	if ( destChar > GetCurrentChar() ) 
	{
		charDistance = destChar - GetCurrentChar();
		goUp = true;
	}
	else
	{
		charDistance = GetCurrentChar() - destChar;
		goUp = false;
	}

	// Walk the way...
	for ( unsigned int codeCharCount = 0; codeCharCount < charDistance; codeCharCount++ )
	{
		if ( goUp )
			IncCharCodeGen();
		else
			DecCharCodeGen();
	}
}

void ABrainfuckCodeGenerator::WalkToPosCodeGen( unsigned int destPos )
{
	bool moveRight = destPos > m_brainfuckBufferPos;
	unsigned int steps = moveRight ? ( destPos - m_brainfuckBufferPos ) : (m_brainfuckBufferPos - destPos);

	for ( unsigned int step = 0; step < steps; step++ )
		moveRight ? MoveRightCodeGen() : MoveLeftCodeGen();
}

char ABrainfuckCodeGenerator::GetCurrentChar()
{
	return m_brainfuckBufferStr[m_brainfuckBufferPos];
}

unsigned int ABrainfuckCodeGenerator::GetPos()
{
	return m_brainfuckBufferPos;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
