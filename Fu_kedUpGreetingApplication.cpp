///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Fu_kedUpGreetingApplication.cpp - Application code of the solution!
//
// The output of this code is a fu*ked-up greeting in brainfuck code
// (Fu_kedUpGreetingBrainfuck.txt). It should be pretty easy to execute in the
// brain of an insane person (like you?).
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <string.h>
#include <tchar.h>
#include <stdio.h>

#include <fstream>
#include <iostream>

#include "BrainfuckCodeGeneratorAbstract.h"
#include "BrainfuckCodeGeneratorFactory.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

using namespace std;

const char FU_KED_UP_GREETING_BRAINFUCK_FILENAME[]	= "Outputs/Fu_kedUpGreetingBrainfuck.txt";
const char FU_KED_UP_GREETING_TEXT_FILENAME[]		= "Outputs/Fu_kedUpGreetingText.txt";

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "Fu_kedUpGreetingApplicationHiddenCode.inl"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int _tmain(int argc, _TCHAR* argv[])
{
	ABrainfuckCodeGenerator* pBfCodeGeneratorTypewriter;
	pBfCodeGeneratorTypewriter = BrainfuckCodeGeneratorFactory::CreateInstance( BrainfuckCodeGeneratorFactory::ALG_TYPE__TYPEWRITER );
	assert(pBfCodeGeneratorTypewriter);

	ABrainfuckCodeGenerator* pBfCodeGeneratorScanner;
	pBfCodeGeneratorScanner = BrainfuckCodeGeneratorFactory::CreateInstance( BrainfuckCodeGeneratorFactory::ALG_TYPE__SCANNER );
	assert(pBfCodeGeneratorScanner);

	// Save code to file
	ofstream textFile;
	textFile.open(FU_KED_UP_GREETING_TEXT_FILENAME);
	
	// Generate code for each line in the fu*ked-up greeting
	for ( unsigned char greetingLineIdx = 0; greetingLineIdx < FU_KED_UP_GREETING_LINES_COUNT; greetingLineIdx++ )
	{
		textFile << g_Fu_kedUpStrings[greetingLineIdx];
		pBfCodeGeneratorTypewriter->AddNexString( g_Fu_kedUpStrings[greetingLineIdx] );
		pBfCodeGeneratorScanner->AddNexString( g_Fu_kedUpStrings[greetingLineIdx] );
	}

	// Close the file...
	textFile.close();

	// Get the generated code
	string strCodeTypewriter = pBfCodeGeneratorTypewriter->GetCodeString();
	string strCodeScanner = pBfCodeGeneratorScanner->GetCodeString();

	string strCodeBest = (strCodeTypewriter.length() < strCodeScanner.length()) ? strCodeTypewriter : strCodeScanner;

	// Save code to file
	ofstream codeFile;
	codeFile.open(FU_KED_UP_GREETING_BRAINFUCK_FILENAME);
	codeFile << strCodeBest;
	codeFile.close();
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
